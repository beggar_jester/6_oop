public class Good {
    final private String Name;
    final private double Price;
    final private double Rating;

    public Good(String name, double price, double rating){
        Name = name;
        Price = price;
        Rating = rating;
    }

    public String getName(){
        return Name;
    }

    public double getPrice() {
        return Price;
    }

    public double getRating() {
        return Rating;
    }

    @Override
    public String toString() {
        return "Good{" +
                "Name='" + Name + '\'' +
                ", Price=" + Price +
                ", Rating=" + Rating +
                '}';
    }
}

import java.util.Arrays;

public class Basket {
    final private Good[] BoughtGoods;

    public Basket(Good[] boughtGoods) {
        BoughtGoods = boughtGoods;
    }

    public Good[] getBoughtGoods()
    {
        return BoughtGoods;
    }

    @Override
    public String toString() {
        return "Basket{" +
                "BoughtGoods=" + Arrays.toString(BoughtGoods) +
                '}';
    }
}

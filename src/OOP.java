public class OOP {
    public static void main(String[] args) {
        Good Book1 = new Good("The stranger", 5.5, 10.0);
        Good Book2 = new Good("Harry Potter and the Chamber of Secrets", 3.3, 9.4);
        Good Book3 = new Good("The Lord of the Rings", 7.0, 7.7);
        Good Film1 = new Good("Sherlock", 9.9, 8.9);
        Good Film2 = new Good("Exam", 3.9, 7.1);
        Good Film3 = new Good("Twilight", 8.5, 6.7);
        Category Books = new Category("Your favourite in library", new Good[]{Book1, Book2, Book3});
        Category Movies = new Category("Your favourite in cinema", new Good[]{Film1, Film2, Film3});
        Basket Entertainments = new Basket(new Good[]{Books.getGoods()[0], Movies.getGoods()[0], Movies.getGoods()[1]});
        User Creator = new User("Raccoon", "777", Entertainments);
        System.out.println(Creator);
    }
}

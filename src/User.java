public class User {
    final private String Login;
    final private String Password;
    final private Basket UserBasket;

    public User(String login, String password, Basket userBasket) {
        Login = login;
        Password = password;
        UserBasket = userBasket;
    }

    public String getLogin() {
        return Login;
    }

    public String getPassword() {
        return Password;
    }

    public Basket getUserBasket() {
        return UserBasket;
    }

    @Override
    public String toString() {
        return "User{" +
                "Login='" + Login + '\'' +
                ", Password='" + Password + '\'' +
                ", UserBasket=" + UserBasket +
                '}';
    }
}

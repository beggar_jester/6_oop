import java.util.Arrays;

public class Category {
    final private String Name;
    final private Good[] Goods;

    public Category(String name, Good[] goods) {
        Name = name;
        Goods = goods;
    }

    public String getName() {
        return Name;
    }

    public Good[] getGoods() {
        return Goods;
    }

    @Override
    public String toString() {
        return "Category{" +
                "Name='" + Name + '\'' +
                ", Goods=" + Arrays.toString(Goods) +
                '}';
    }
}
